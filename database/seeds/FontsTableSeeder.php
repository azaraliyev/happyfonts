<?php

use Illuminate\Database\Seeder;

class FontsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('fonts')->delete();
        $json = File::get('database/data/fonts.json');
        $data = json_decode($json);
        foreach ($data as $key=>$obj) {
            $cat = \App\FontCategory::where('name', '=', str_replace('-', ' ', title_case($obj->category)))->get()->first()->id;
            $link = 'https://fonts.google.com/specimen/' . str_replace(' ', '+', $obj->title);
            \App\Font::create(array(
                'name' => $obj->title,
                'preview' => $obj->preview_text,
                'category_id' => $cat,
                'link' => $link
            ));
        }
    }
}
