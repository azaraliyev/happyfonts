<?php

use Illuminate\Database\Seeder;

class FontCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('font_categories')->delete();
        $json = File::get('database/data/categories.json');
        $data = json_decode($json);
        foreach ($data as $obj) {
            \App\FontCategory::create(array(
                'name' => $obj->name,
            ));
        }
    }
}
