let mix = require('laravel-mix');
let webpack = require('webpack');
let path = require('path');

mix
  .js('resources/assets/js/app.js', 'public/js')
  .js('resources/assets/js/dashboard.js', 'public/js')
  .sass('resources/assets/sass/app.scss', 'public/css')
  .sass('resources/assets/sass/dashboard.scss', 'public/css')
  .webpackConfig({
    resolve: {
      extensions: ['.js', '.vue', '.json'],
      alias: {
        '@': path.resolve('resources/assets/js/')
      }
    }
  });

if (mix.inProduction()) {
  mix.version();
}