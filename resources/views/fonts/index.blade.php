@extends('layouts.app')

@section('content')
<div class="uk-container">
        <div class="uk-flex uk-flex-center" uk-grid>
            <div class="uk-width-3-5@m">
                <ul class="uk-list uk-list-divider">
                    @foreach($fonts as $font)
                        <li>{{ $font->name }}</li>
                        @if (Auth::check())
                            <favorite
                                :font="{{ $font->id }}"
                                :favorited="{{ $font->favorited() ? true : false }}">    
                            </favorite>
                        @endif
                    @endforeach
                </ul>
                {{ $fonts->links() }}
            </div>
        </div>
</div>

@endsection
