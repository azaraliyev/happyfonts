@extends('layouts.app')

@section('content')
    <typefaces></typefaces>
    <dashboard
        authorized="{{ !Auth::guest() }}">
    </dashboard>
@endsection
