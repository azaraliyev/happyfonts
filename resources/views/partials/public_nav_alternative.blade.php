<div id="login-modal" class="uk-modal-full" uk-modal>
    <div class="uk-modal-dialog jis-modal-dark">
         <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
         <div class="uk-flex uk-flex-middle uk-text-center uk-flex-center" uk-height-viewport>
             <login
                route="{{ route('login') }}"
                password_forget_route="{{ route('password.request') }}"
                :dev_mode="false">
            </login>
         </div>
    </div>
</div>

<div id="register-modal" class="uk-modal-full" uk-modal>
    <div class="uk-modal-dialog  jis-modal-yellow">
         <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
         <div class="uk-flex uk-flex-middle uk-text-center uk-flex-center" uk-height-viewport>
             <registration
                route="{{ route('register') }}"
                :dev_mode="false"
                >
            </registration>
         </div>
    </div>
</div>
