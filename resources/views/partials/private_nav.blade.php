<div uk-navbar>
    <div class="uk-text-center uk-align-center  uk-align-left@m">
        <ul class="uk-subnav uk-subnav-divider" uk-margin>
            <li>
                <a onclick="app.$store.dispatch('showCategoriesHandler')">
                    <span class="uk-icon uk-margin-small" uk-icon="icon: jcategories; ratio: 0.6"></span>
                    <br>
                    @{{ $store.getters.category }}
                </a>
            </li>

            <li>
                <a onclick="app.$refs.magical.refresh()" uk-toggle="target: #favorites-modal">
                    <span class="uk-icon uk-margin-small favorites" uk-icon="icon: jstarfilled; ratio: 0.6"></span>
                    <br>
                    Favorites
                    <br>
                    <span style="position: absolute; left: 70px" class="uk-badge">@{{ $store.getters.favoritesCount }}</span>
                </a>
            </li>

            <li>
                <a href="#"><span class="uk-icon uk-margin-small" uk-icon="icon: jlogin; ratio: 0.55"></span>
                <br>{{ strtoupper(Auth::user()->name) }} <span uk-icon="icon:  triangle-down"></span></a>
                <div uk-dropdown="mode: click;">
                    <ul class="uk-nav uk-dropdown-nav uk-text-left">
                        <!-- <li><a href="#"><span class="uk-icon uk-margin-small" uk-icon="icon: settings"></span> Settings</a></li> -->
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                    <span class="uk-icon uk-margin-small" uk-icon="icon: sign-out"></span>
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>

<div id="favorites-modal" class="uk-modal-full" uk-modal>
    <div class="uk-modal-dialog jis-modal-white">
         <button class="uk-modal-close-full uk-close-large" type="button" uk-close></button>
         <div class="uk-padding-large" uk-height-viewport>
            <div class="uk-flex uk-flex-center" uk-grid>
                <div class="uk-width-2-5@m">
                    <favorites ref="magical"></favorites>     
                </div>
            </div>
         </div>
    </div>
</div>