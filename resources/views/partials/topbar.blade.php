<div class="uk-clearfix top">
    <div class="uk-float-left">
        <a class="logo" href="{{ url('/') }}">
            <img src="{{url('/images/logo.png')}}" alt="logo"/>
        </a>
    </div>
    <div class="uk-float-left ss">
        <div class="site-credit uk-margin-medium-left">
            <span>by</span>
            <img src="{{url('/images/jis.svg')}}" alt="JIS logo"/>
        </div>
    </div>
</div>