<div class="uk-clearfix top">
    <a class="logo" href="{{ url('/') }}">
        <img src="{{url('/images/logo.png')}}" alt="logo"/>
    </a>
    <div class="site-credit">
        <strong>by</strong>
        <img src="{{url('/images/jis.svg')}}" alt="JIS logo"/>
    </div>
</div>