@extends('layouts.app')

@section('content')

    <forgot-password
        route="{{ route('password.email') }}"
        :dev_mode="true">
    </forgot-password>

@endsection
