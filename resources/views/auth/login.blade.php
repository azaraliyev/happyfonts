@extends('layouts.app')

@section('content')
    <div class="login-page uk-padding-large">
        <div class="uk-flex uk-flex-middle uk-text-center uk-flex-center">
            <login
                route="{{ route('login') }}"
                password_forget_route="{{ route('password.request') }}"
                :dev_mode="false">
            </login>
        </div>
    </div>
@endsection
