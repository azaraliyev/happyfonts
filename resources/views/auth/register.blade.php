@extends('layouts.app')

@section('content')

    <registration
        route="{{ route('register') }}"
        :dev_mode="false">
    </registration>
    
@endsection
