@extends('layouts.public')

@section('content')
        <div class="uk-margin"></div>
        <div class="uk-flex uk-flex-center" uk-grid>
            <div class="uk-width-3-5@m">
                <div class="uk-card uk-card-body">
                    <h1 class="uk-heading-line uk-text-left">
                        <span>
                        {{ $user->name }}
                        </span>
                    </h1>
                    <p class="uk-text-meta uk-text-left">Favorite Fonts</p>
                    <ul class="uk-list uk-list-divider uk-list-large">
                        @forelse ($favorites as $favorite)
                            <li>
                                <p>{{ $favorite->name }}</p>
                                <a style="border-radius:100px" href="{{ $favorite->link }}" target="_blank" class="uk-button uk-button-primary">Link</a>
                            </li>
                        @empty
                            <p class="uk-text-center">This user has no favorites</p>
                        @endforelse
                    </ul>
                    {{ $favorites->links() }}
                </div>
            </div>
        </div>
@endsection
