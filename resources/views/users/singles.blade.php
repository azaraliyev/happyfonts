<ul class="uk-list uk-list-divider uk-list-large">
    @forelse ($users as $user)
        <li>
            <p>{{ $user->name }}</p>
            <a style="border-radius:100px" href="/profiles/{{ $user->id }}" target="_blank" class="uk-button uk-button-primary">Profile</a>
        </li>
    @empty
        <p class="uk-text-center">There is no public user!</p>
    @endforelse
</ul>
{{ $users->links() }}