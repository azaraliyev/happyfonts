@extends('layouts.public')

@section('content')
        <div class="uk-margin"></div>
        <div class="uk-flex uk-flex-center" uk-grid>
            <div class="uk-width-3-5@m">
                <div class="uk-card uk-card-body">
                    <h1 class="uk-heading-line uk-text-left">
                        <span>
                        Public Users
                        </span>
                    </h1>
                    <div class="profiles">
                    <ul class="uk-list uk-list-divider uk-list-large">
                        @forelse ($users as $user)
                            <li>
                                <p>{{ $user->name }}</p>
                                <a style="border-radius:100px" href="/profiles/{{ $user->id }}" target="_blank" class="uk-button uk-button-primary">Profile</a>
                            </li>
                        @empty
                            <p class="uk-text-center">There is no public user!</p>
                        @endforelse
                    </ul>
                    {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
@endsection
