@extends('layouts.app')

@section('content')
    <div class="uk-container">
        <div class="uk-flex uk-flex-center" uk-grid>
            <div class="uk-width-3-5@m">
                <favorites></favorites>     
            </div>
        </div>
    </div>
@endsection
