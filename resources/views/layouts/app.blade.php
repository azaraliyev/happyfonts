<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
</head>
<body>

    <div id="app">

        <div>
            @if (Request::is('/'))
                @if (Auth::guest())
                    @include('partials.public_nav_alternative')
                @endif
            @else
                @if (Auth::guest())
                    @include('partials.public_nav')
                @endif
            @endif
        </div>

        @yield('content')
        
    </div>

    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
    <script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
