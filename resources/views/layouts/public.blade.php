<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style type="text/css">
        html, body {
            background-color: #ffcb08;
            padding: 0!important;
        }
        ul.pagination {
            list-style: none;
            margin-top: 40px;
            padding: 0;
        }
        ul.pagination li {
            display: inline-block;
            padding: 10px;
            margin: 0;
        }
        .uk-clearfix.top {
            display: inline-block;
            padding: 20px;
        }
        .uk-card {
            background-color: white;
            border-radius: 20px;
        }
        .uk-heading-line>:after, .uk-heading-line>:before {
            border-bottom-color: #dfdfdf;
        }
        .uk-list-divider>li:nth-child(n+2) {
            border-top: 1px dashed #dfdfdf;
        }
        .site-credit {
            color:  #222;
            text-transform: uppercase;
            padding-top: 10px;
        }
    </style>
</head>
<body>

    <div id="app">

        <div class="uk-section">
            <div class="uk-container">
                <div class="uk-text-center">
                    <div>
                        @include('partials.topbar_small')
                    </div>
                </div>
                @yield('content')
            </div>
        </div>
        
    </div>

    <!-- Scripts -->

    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $(window).on('hashchange', function() {
            if (window.location.hash) {
                var page = window.location.hash.replace('#', '');
                if (page == Number.NaN || page <= 0) {
                    return false;
                } else {
                    getProfiles(page);
                }
            }
        });
        $(document).ready(function() {
            $(document).on('click', '.profiles .pagination a', function (e) {
                getProfiles($(this).attr('href').split('page=')[1]);
                e.preventDefault();
            });
        });
        function getProfiles(page) {
            $.ajax({
                url : '?page=' + page,
                dataType: 'json',
            }).done(function (data) {
                $('.profiles').html(data);
                location.hash = page;
                $("html, body").animate({ scrollTop: $('.uk-card-body').offset()['top'] - 10 + 'px' }, 0);
            }).fail(function () {
                alert('Profiles could not be loaded.');
            });
        }
    </script>
</body>
</html>
