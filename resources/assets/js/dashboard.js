require('@/bootstrap');

import Vue from 'vue'

import Private from '@/components/Private'

new Vue({
    el: '#app',
    components: {
        Private
    }
});
