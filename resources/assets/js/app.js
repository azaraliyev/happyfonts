require('@/bootstrap');

import Vue from 'vue'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VeeValidate from 'vee-validate'

Vue.use(VueAwesomeSwiper)
Vue.use(VeeValidate)

window.UIkit = require('uikit/dist/js/uikit-core');
import Icons from 'uikit/dist/js/uikit-icons';
import Tooltip from 'uikit/dist/js/components/tooltip';
import Notification from 'uikit/dist/js/components/notification';
import Parallax from 'uikit/dist/js/components/parallax';
UIkit.use(Icons);
UIkit.use(Tooltip);
UIkit.use(Notification);
UIkit.use(Parallax);

import store from '@/store'

import Dashboard from '@/components/Dashboard.vue';
import Favorite from '@/components/Favorite.vue';
import Favorites from '@/components/Favorites.vue';
import List from '@/components/List.vue';
import Typefaces from '@/components/Typefaces.vue';
import Login from '@/components/auth/Login.vue';
import Registration from '@/components/auth/Register.vue';
import ForgotPassword from '@/components/auth/ForgotPassword.vue';

window.app = new Vue({
    el: '#app',
    store: store,
    components: {
        Dashboard,
        Favorite,
        Favorites,
        Login,
        Registration,
        ForgotPassword,
        Typefaces,
        List
    }
});
