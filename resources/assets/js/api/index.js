let getTypefaces = () => {
    return axios.get('/api/categories')
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error)
        })
}

let getFontsByTypeface = (typefaceId) => {
    return axios.get(`/api/categories/${typefaceId}`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error)
        })
}

let getFontsRandomly = () => {
    return axios.get('/api/categories/random')
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error)
        })
}

let getFavorites = () => {
    return axios.get('/favorites')
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error)
        })
}

let addFavorite = (fontId) => {
    return axios.post(`/favorite/${fontId}`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error)
        })
}

let removeFavorite = (fontId) => {
    return  axios.post(`/unfavorite/${fontId}`)
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error)
        })
}

let freedom = (url) => {
    return axios.get(url)
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(error)
        })
}

let savePositions = (fonts) => {
    return axios.post('/save-positions', { fonts })
        .then(response => {
            return response.data
        })
        .catch(error => {
            console.log(error)
        })
}

export default {
    getTypefaces,
    getFontsByTypeface,
    getFavorites,
    addFavorite,
    removeFavorite,
    getFontsRandomly,
    freedom,
    savePositions
}
