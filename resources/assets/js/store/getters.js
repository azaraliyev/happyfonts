export default {
    typefaces: state => state.typefaces,
    fonts: state => state.fonts,
    favorites: state => state.favorites,
    status: state => state.typefacesShown,
    favoritesCount: state => state.favorites.length,
    category: state => state.category,
    preview: state => state.preview,
    nextUrl: state => state.nextUrl,
}
