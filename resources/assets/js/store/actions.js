import * as types from './types'
import API from '@/api'

const loadGoogleFonts = (fonts) => {
    let names = fonts.collection.data.map(font => font.name);
    WebFont.load({
        google: {
            families: names
        },
        loading: () => {
        },
        active: () => {
        },
        fontinactive: () => {
        },
        classes: false
    })
}

export default {

    receiveTypefaces ({ commit }) {
        API.getTypefaces().then(typefaces => {
            commit(types.RECEIVE_TYPEFACES, { typefaces })
        })
    },

    receiveFonts ({ commit }, typefaceId) {
        API.getFontsByTypeface(typefaceId).then(fonts => {
            commit(types.RECEIVE_FONTS, { fonts })
            loadGoogleFonts(fonts);
        })
    },

    mergeFonts ({ commit }, fonts) {
        commit(types.MERGE_FONTS, { fonts });
        loadGoogleFonts(fonts);
    },

    receiveRandomFonts({ commit }) {
        API.getFontsRandomly().then(fonts => {
            commit(types.RECEIVE_FONTS, { fonts });
            loadGoogleFonts(fonts);
        })
    },

    receiveFavorites ({ commit }) {
        API.getFavorites().then(favorites => {
            commit(types.RECEIVE_FAVORITES, { favorites })
        })
    },

    addFavorite ({ commit }, font) {
        API.addFavorite(font.id).then(response => {
            if (response.success) {
                commit(types.ADD_FAVORITE, font)
            } else {
                alert('You have this font in your favorites')
            }
        })
    },

    removeFavorite ({ commit }, font) {
        API.removeFavorite(font.id).then(response => {
            commit(types.REMOVE_FAVORITE, font)
        })
    },

    showCategoriesHandler ({ commit }) {
        commit(types.TOGGLE_TYPEFACES_UI)
    },

    updatePreview ({ commit }, text) {
        commit(types.UPDATE_PREVIEW, text)
    }
    
  }
