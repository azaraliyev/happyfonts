import * as types from './types'

export default {

    [types.RECEIVE_TYPEFACES] (state, { typefaces }) {
        state.typefaces = typefaces
    },

    [types.TOGGLE_TYPEFACES_UI] (state) {
        state.typefacesShown = !state.typefacesShown
    },

    [types.RECEIVE_FONTS] (state, { fonts }) {
        let favorites = state.favorites
        fonts.collection.data.map(font => {
            if (_.some(favorites, font)) {
                font.favorited = true
            } else {
                font.favorited = false
            }
        })
        state.fonts = fonts.collection.data
        state.category = fonts.category.name
        state.nextUrl = fonts.collection.next_page_url
        state.total = fonts.collection.total
    },

    [types.MERGE_FONTS] (state, { fonts }) {
        let favorites = state.favorites
        fonts.collection.data.map(font => {
            if (_.some(favorites, font)) {
                font.favorited = true
            } else {
                font.favorited = false
            }
        })
        state.fonts = state.fonts.concat(fonts.collection.data)
        state.nextUrl = fonts.collection.next_page_url
    },

    [types.RECEIVE_FAVORITES] (state, { favorites }) {
        state.favorites = favorites
    },

    [types.ADD_FAVORITE] (state, font) {
        state.favorites.unshift(font)
        state.fonts.find(f => f.id === font.id).favorited = true
    },

    [types.REMOVE_FAVORITE] (state, font) {
        let favorites = state.favorites.filter(favorite => {
            return favorite.id !== font.id
        })
        state.favorites = favorites
        state.fonts.find(f => f.id === font.id).favorited = false
    },

    [types.UPDATE_PREVIEW] (state, text) {
        state.preview = text
    }

  }
 