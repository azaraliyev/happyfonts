export default {
    typefaces: [],
    fonts: [],
    favorites: [],
    typefacesShown: false,
    category: 'categories',
    loading: false,
    preview: '',
    nextUrl: null,
    total: 0,
}
