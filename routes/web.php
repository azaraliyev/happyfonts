<?php

Route::get('/', 'PagesController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/favorites', 'UsersController@favorites')->middleware('auth');
Route::post('/favorite/{font}', 'FontsController@favoriteFont');
Route::post('/unfavorite/{font}', 'FontsController@unFavoriteFont');


Route::get('/profiles', 'UsersController@profiles');
Route::get('/profiles/{user}', 'UsersController@profile');

Route::get('/dashboard', 'DashboardController@index');
Route::post('/save-positions', 'FontsController@savePositions');
Route::get('/categories/{category}/list', 'FontCategoriesController@list')->name('categorywithlist');

Route::get('/test', function() {
    return view('test');
});
