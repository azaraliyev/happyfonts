<?php

use Illuminate\Http\Request;

Route::get('/categories', 'FontCategoriesController@index')->name('categories');
Route::get('/categories/{category}', 'FontCategoriesController@show')->name('category');