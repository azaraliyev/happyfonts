<?php

namespace App;

use App\Favorite;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Font extends Model
{

    protected $fillable = [
        'name',
        'rank',
        'preview',
    ];

    public function category()
    {
        return $this->belongsTo('App\FontCategory');
    }

    public function favorited()
    {
        return (bool) Favorite::where('user_id', Auth::id())->where('font_id', $this->id)->first();
    }

}
