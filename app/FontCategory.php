<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FontCategory extends Model
{

    protected $fillable = [
        'name',
    ];
    protected $appends = [
        'url',
    ];

    // Model Relationships
    public function fonts()
    {
        return $this->hasMany('App\Font', 'category_id');
    }

    // Model Attributes
    public function getUrlAttribute()
    {
        return route('category', [$this->id]);
    }

    // Model Local Scopes
    public function scopeCategories($query)
    {
        return $query->orderBy('name', 'desc')->select('id', 'name')->get();
    }

}
