<?php

namespace App\Jobs;

use App\User;
use App\Font;
use App\Position;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CreateUserFontsListing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach (Font::all() as $key=>$value) {
            $font = Position::updateOrCreate(
                ['font_id' => (int) $value->id, 'user_id' => $this->user->id],
                ['position' => (int) $key]
            );
        }
    }
}
