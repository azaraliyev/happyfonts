<?php

namespace App\Http\Controllers;

use App\User;
use App\Font;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsersController extends Controller
{

    public function profiles(Request $request)
    {
        $users = User::where('make_public', true)->simplePaginate(10);

        if ($request->ajax()) {
            return response()->json(view('users.singles', array('users' => $users))->render());
        }

        return view('users.profiles', compact('users'));
    }

    public function profile(User $user)
    {
        if ($user->make_public) {
            $favorites = $user->favorites()->orderBy('favorites.created_at', 'desc')->simplePaginate(5);
            return view('users.profile', compact('user', 'favorites'));
        } else {
            return back();
        }
    }

    /**
     * Return current user's favorites
     * 
     * @return \Illuminate\Http\Response
     */
    public function favorites(Request $request)
    {
        $favorites = Auth::user()->favorites()->with('category')->get();
        
        if($request->ajax()){
            return $favorites;
        }

        return view('users.favorites', compact('favorites'));
    }
}
