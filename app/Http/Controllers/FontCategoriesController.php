<?php

namespace App\Http\Controllers;

use App\FontCategory;
use App\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FontCategoriesController extends Controller
{
    /**
     * Display a listing of the font categories.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(
            FontCategory::categories()
        );
    }

    /**
     * Show the form for creating a new font category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created font category in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified font category with fonts.
     *
     * @param  \App\FontCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function show(FontCategory $category)
    {
        return response()->json([
            'category' => $category,
            'collection' => $category
                ->fonts()
                ->select(['id', 'name', 'preview', 'link'])
                ->paginate(20)
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FontCategory  $fontCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(FontCategory $fontCategory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FontCategory  $fontCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FontCategory $fontCategory)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FontCategory  $fontCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(FontCategory $fontCategory)
    {
        //
    }

    public function list(FontCategory $category)
    {
        return response()->json([
            'category' => $category,
            'collection' => Auth::user()->fonts($category->id)->get()
        ]);
        
        
    }

}
