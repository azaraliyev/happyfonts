<?php

namespace App\Http\Controllers;

use App\Font;
use App\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FontsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fonts = Font::simplePaginate(10);
        return view('fonts.index', compact('fonts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Font  $font
     * @return \Illuminate\Http\Response
     */
    public function show(Font $font)
    {
        return $font;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Font  $font
     * @return \Illuminate\Http\Response
     */
    public function edit(Font $font)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Font  $font
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Font $font)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Font  $font
     * @return \Illuminate\Http\Response
     */
    public function destroy(Font $font)
    {
        //
    }

    /**
     * Add the specified font to current user's favorites
     * 
     * @param  \App\Font $font
     * @return \Illuminate\Http\Response
     */
    public function favoriteFont(Font $font)
    {
        Auth::user()->favorites()->attach($font->id);

        return ["success" => true];
    }

    /**
     * Remove the specified font from current user's favorites
     * @param  \App\Font $font
     * @return \Illuminate\Http\Response
     */
    public function unFavoriteFont(Font $font)
    {
        $res = Auth::user()->favorites()->detach($font->id);

        return $res;
    }

    public function savePositions(Request $request) {
        $user_id = Auth::user()->id;
        foreach ($request->fonts as $value) {
            $font = Position::updateOrCreate(
                ['font_id' => (int) $value['font_id'], 'user_id' => $user_id],
                ['position' => (int) $value['position']]
            );
        }
        return 'success';
    }
}
